from rest_framework import serializers
from django.contrib.auth.models import User

from berita.models import kategori, Artikel
from pengguna.models import Biodata

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email']

class BiodataSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    class Meta:
        model = Biodata
        fields = ['user', 'alamat', 'telepon', 'foto']


class kategoriSerializer(serializers.ModelSerializer):
    class Meta:
        model = kategori
        fields = ['id', 'nama',]

class ArtikelSerializer(serializers.ModelSerializer):
    Kategori = serializers.PrimaryKeyRelatedField(queryset=kategori.objects.all())
    author = serializers.PrimaryKeyRelatedField(queryset=kategori.objects.all())

    kategori_detail = kategoriSerializer(source='kategori', read_only=True)
    author_detail = kategoriSerializer(source='kategori', read_only=True)

    class Meta:
        model = Artikel
        fields = ['id', 'judul', 'isi', 'kategori', 'author', 'thumbnail', 'created_at', 'slug']