from django.shortcuts import render #untuk memamnggil file html
from django.http import HttpResponse #format html langsung ditulis dalam HttpResponse
from berita.models import Artikel, kategori

def home(request):
    template_name = 'halaman/index.html'
    kategori_nama = request.GET.get('kategori')
    if kategori_nama is None:
        print("ALL")
        data_artikel = Artikel.objects.all()
        menu_aktif = "ALL"
    else:
        print("Bukan ALL")
        try:
            get_kategori = kategori.objects.get(nama=kategori_nama)
            data_artikel = Artikel.objects.filter(kategori=get_kategori)
            menu_aktif = get_kategori.nama
        except kategori.DoesNotExist:
            menu_aktif = "ALL"
            data_artikel = []


    data_kategori = kategori.objects.all()
    context = {
        'title' : 'Selamat datang',
        'data_artikel' : data_artikel,
        'data_kategori' : data_kategori,
        'menu_aktif' : menu_aktif,
    }
    return render(request, template_name, context)


def about(request):
    template_name = 'halaman/about.html'
    context = {
        'title' : 'tentang saya',
        'welcome' : 'selamat datang dihalaman tentang saya',
    }
    return render(request, template_name, context)

def about2(request):
    template_name = 'about2.html'
    context = {
        'title': 'Tentang Saya Versi 2',
        'welcome': 'Selamat datang di halaman tentang saya versi 2',
    }
    return render(request, template_name, context)

def contact(request):
    template_name = 'halaman/contact.html'
    context = {
        'title': 'Tentang Saya Versi 2',
        'welcome': 'Selamat datang di halaman tentang saya versi 2',
    }
    return render(request, template_name, context)

def detail_artikel(request, slug):
    template_name = 'halaman/detail_artikel.html'
    artikel = Artikel.objects.get(slug = slug)
    print(artikel)
    context ={
        'title': artikel.judul,
        'artikel': artikel,
    }
    return render(request, template_name, context)

def calendar_view(request):
    template_name = 'dashboard/calendar.html'
    context = {
        'title': 'Kalender',
    }
    return render(request, template_name, context)